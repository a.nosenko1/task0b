#include "TokenService.h"
#include <cstring>

TokenService::TokenService(const string &line) {
    this->line = line;
    findAllWords();
}

bool TokenService::getNextWord(string *word) {
    if (words_count) {
        *word = words[word_number++];
        if (word_number == words_count)
            words_count = 0;
        return true;
    }
    return false;
}

void TokenService::findAllWords() {
    char *l = const_cast<char *>(line.c_str());
    char *pch = strtok(l, " ,.-/^*;:()!?\t\n\0\r");
    while (pch != nullptr) {
        if (strcmp(pch, "\r") != 0)
            words.emplace_back(pch);
        pch = strtok(nullptr, " ,.-/^*;:()!?\t\n\0\r");
    }
    words_count = words.size();
}

void TokenService::setNewLine(const string &line_new) {
    this->line = line_new;
    words.clear();
    words_count = word_number = 0;
    findAllWords();
}
