#include <iostream>
#include <string>
#include "FileReader.h"
#include "TokenService.h"
#include "ReportService.h"
#include "WordStatService.h"

using namespace std;

int main(int argc, char* argv[]) {
//    if (argc != 3){
//        cout << "incorrect number of argumets" << endl;
//        return 0;
//    }
    FileReader file("bible.txt");
    WordStatService library;
    string next_line;
    while (file.getNextline(&next_line)) {
        TokenService new_line(next_line);
        string next_word;
        while (new_line.getNextWord(&next_word)) {
            library.addWord(next_word);
        }
    }
    ReportService report("output.csv", library.getDict());
    report.write();
}

