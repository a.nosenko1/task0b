#ifndef TASK0B_FILEREADER_H
#define TASK0B_FILEREADER_H

#include <string>
#include <fstream>

using namespace std;
#endif

class FileReader {
public:

    explicit FileReader(string file_name);

    ~FileReader();

    bool getNextline(string *line);

    void reset(string new_file_name); // переделать на reset

private:
    string file_name;
    ifstream file;

    void fileOpen();

    void fileClose();
};

