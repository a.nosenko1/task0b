#include <string>
#include <iostream>
#include "FileReader.h"

using namespace std;

FileReader::FileReader(string file_name) {
    this->file_name = std::move(file_name);
    fileOpen();
}

FileReader::~FileReader() {
    fileClose();
}

bool FileReader::getNextline(string *line) {
    if (file.is_open()) {
        if (getline(file, *line)) {
            return true;
        } else {
            fileClose();
            return false;
        }
    }
    cout << "file opening error" << endl;
    exit(0);
    // return false;
}

void FileReader::fileOpen() {
    file.open(file_name);
    if (!file.is_open()) {
        cerr << "file opening error\n";
        exit(23);
    }
}

void FileReader::fileClose() {
    if (file.is_open())
        file.close();
}

void FileReader::reset(string new_file_name) {
    fileClose();
    this->file_name = std::move(new_file_name);
    fileOpen();
}


